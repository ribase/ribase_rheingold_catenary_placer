local poles = {}
poles.poles = {}

function poles.createPoles(params)
    local trackDistanceModifier = -5.0
    local trackDistance = -0.05
    local poleDistance = -0.05
    local offset_both
    local offset
    local counter
    local result = { }
    result.models = { }

    if params.ribase_catenary_offset_x == 0 then offset = 1 end;
    if params.ribase_catenary_offset_x == 1 then offset = 0.75 end;
    if params.ribase_catenary_offset_x == 2 then offset = 0.60 end;
    if params.ribase_catenary_offset_x == 3 then offset = 0.45 end;
    if params.ribase_catenary_offset_x == 4 then offset = 0.30 end;
    if params.ribase_catenary_offset_x == 5 then offset = 0.15 end;
    if params.ribase_catenary_offset_x == 6 then offset = 0 end;
    if params.ribase_catenary_offset_x == 7 then offset = -0.15 end;
    if params.ribase_catenary_offset_x == 8 then offset = -0.30 end;
    if params.ribase_catenary_offset_x == 9 then offset = -0.35 end;
    if params.ribase_catenary_offset_x == 10 then offset = -0.60 end;
    if params.ribase_catenary_offset_x == 11 then offset = -0.75 end;
    if params.ribase_catenary_offset_x == 12 then offset = -1 end;

    if params.offset_both == 0 then offset_both = 0.75 end;
    if params.offset_both == 1 then offset_both = 0.60 end;
    if params.offset_both == 2 then offset_both = 0.45 end;
    if params.offset_both == 3 then offset_both = 0.30 end;
    if params.offset_both == 4 then offset_both = 0.15 end;
    if params.offset_both == 5 then offset_both = 0 end;
    if params.offset_both == 6 then offset_both = -0.15 end;
    if params.offset_both == 7 then offset_both = -0.30 end;
    if params.offset_both == 8 then offset_both = -0.35 end;
    if params.offset_both == 9 then offset_both = -0.60 end;
    if params.offset_both == 10 then offset_both = -0.75 end;

    if params.pos == 1 or params.poles == 4 then
        poleDistance = poleDistance + (-1.25)
    end

    trackDistance = trackDistance + offset_both
    poleDistance = poleDistance + offset_both

    offset = offset - 0.15

    if params.poles == 0 and params.art ~= 7 or params.poles == 4 and params.art ~= 7 or params.poles == 2 and params.art ~= 7 or params.poles == 5 and params.art ~= 7 then

        if params.art ~= 3 then
            counter = params.tracks
            if params.tracks > 2 or params.poles == 4 then
                counter = counter -1
                poleDistance = poleDistance + trackDistanceModifier
                trackDistance = trackDistance + trackDistanceModifier
            end
            --- 1 Gleis ---
            if params.tracks == 1 and params.poles == 5 then
                if params.art == 0 then
                    result.models[#result.models+1] =
                    { id = "rheingold_einzelmast_standard.mdl",
                        transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                elseif params.art == 1 then
                    result.models[#result.models+1] =
                    { id = "rheingold_turmmast_1_5.mdl",
                        transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                elseif params.art == 2 then
                    result.models[#result.models+1] =
                    { id = "rheingold_einzelmast_beton_1_2.mdl",
                        transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                elseif params.art == 3 then
                    result.models[#result.models+1] =
                    { id = "rheingold_turmmast_2_5.mdl",
                        transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                elseif params.art == 4 then
                    result.models[#result.models+1] =
                    { id = "rheingold_bruecke_einzelmast_standard.mdl",
                        transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                elseif params.art == 5 then
                    result.models[#result.models+1] =
                    { id = "rheingold_einzelmast_altbau_v1.mdl",
                        transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                elseif params.art == 6 then
                    result.models[#result.models+1] =
                    { id = "empty.mdl",
                        transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                end
                if params.pos == 0 and params.art ~= 5 and params.art ~= 2 then
                    if params.type == 0 then
                        result.models[#result.models+1] =
                        { id = "rheingold_ausleger_gross_braun_v1.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                    elseif params.type == 1 then
                        result.models[#result.models+1] =
                        { id = "rheingold_ausleger_gross_braun_v3.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                    end
                elseif params.pos == 1 and params.art ~= 5 and params.art ~= 2 then
                    if params.type == 0 then
                        result.models[#result.models+1] =
                        { id = "rheingold_rohrausleger_kurz_v1_18ff5555.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                    elseif params.type == 1 then
                        result.models[#result.models+1] =
                        { id = "rheingold_rohrausleger_kurz_v3_trans.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                    end
                elseif params.pos == 0 and params.art == 2 then
                    if params.type == 0 then
                        result.models[#result.models+1] =
                        { id = "rheingold_ausleger_gross_gruen_v1.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                    elseif params.type == 1 then
                        result.models[#result.models+1] =
                        { id = "rheingold_ausleger_gross_gruen_v3.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                    end
                end
                if params.poles == 5 and params.tracks == 1 then
                    if params.pos == 1 then
                        if params.art == 0 then
                            result.models[#result.models+1] =
                            { id = "rheingold_einzelmast_standard.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*4 + (1.25) + 7.55, 0.55, 1, } }
                        elseif params.art == 1 then
                            result.models[#result.models+1] =
                            { id = "rheingold_turmmast_1_5.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*4 + (1.25) + 7.55, 0.55, 1, } }
                        elseif params.art == 2 then
                            result.models[#result.models+1] =
                            { id = "rheingold_einzelmast_beton_1_2.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*4 + (1.25) + 7.55, 0.55, 1, } }
                        elseif params.art == 4 then
                            result.models[#result.models+1] =
                            { id = "rheingold_bruecke_einzelmast_standard.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*4 + (1.25) + 7.55, 0.55, 1, } }
                        elseif params.art == 5 then
                            result.models[#result.models+1] =
                            { id = "rheingold_einzelmast_altbau_v1.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*4 + (1.25) + 7.55, 0.55, 1, } }
                        elseif params.art == 6 then
                            result.models[#result.models+1] =
                            { id = "empty.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*4 + (1.25) + 7.55, 0.55, 1, } }
                        end

                        if params.type == 0 then
                            result.models[#result.models+1] =
                            { id = "rheingold_rohrausleger_kurz_v1_18ff5555_mirror.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*2 + trackDistanceModifier/2 + 7.55, 0.55, 1, } }
                        elseif params.type == 1 then
                            result.models[#result.models+1] =
                            { id = "rheingold_rohrausleger_kurz_v3_trans_mirror.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*2 + trackDistanceModifier/2 + 7.55, 0.55, 1, } }
                        end

                    else
                        if params.art == 0 then
                            result.models[#result.models+1] =
                            { id = "rheingold_einzelmast_standard.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*4 + 7.55, 0.55, 1, } }
                        elseif params.art == 1 then
                            result.models[#result.models+1] =
                            { id = "rheingold_turmmast_1_5.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*4 + 7.55, 0.55, 1, } }
                        elseif params.art == 2 then
                            result.models[#result.models+1] =
                            { id = "rheingold_einzelmast_beton_1_2.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*4 + 7.55, 0.55, 1, } }
                        elseif params.art == 4 then
                            result.models[#result.models+1] =
                            { id = "rheingold_bruecke_einzelmast_standard.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*4 + 7.55, 0.55, 1, } }
                        elseif params.art == 5 then
                            result.models[#result.models+1] =
                            { id = "rheingold_einzelmast_altbau_v1.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*4 + 7.55, 0.55, 1, } }
                        elseif params.art == 6 then
                            result.models[#result.models+1] =
                            { id = "empty.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*4 + 7.55, 0.55, 1, } }
                        end
                        if params.type == 0 then
                            result.models[#result.models+1] =
                            { id = "rheingold_ausleger_gross_blau_v1_mirror.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*2 + trackDistanceModifier/2 + 7.55, 0.55, 1, } }
                        elseif params.type == 1 then
                            result.models[#result.models+1] =
                            { id = "rheingold_ausleger_gross_braun_v3_mirror.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistanceModifier*2 + trackDistanceModifier/2 + 7.55, 0.55, 1, } }
                        end
                    end
                end
            end
            if params.tracks == 1 and params.poles == 4 then

                    if params.art == 0 then
                        result.models[#result.models+1] =
                        { id = "rheingold_einzelmast_standard.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                    elseif params.art == 1 then
                        result.models[#result.models+1] =
                        { id = "rheingold_turmmast_1_5.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                    elseif params.art == 2 then
                        result.models[#result.models+1] =
                        { id = "rheingold_einzelmast_beton_1_2.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                    elseif params.art == 4 then
                        result.models[#result.models+1] =
                        { id = "rheingold_bruecke_einzelmast_standard.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                    elseif params.art == 5 then
                        result.models[#result.models+1] =
                        { id = "rheingold_einzelmast_altbau_v1.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                    elseif params.art == 6 then
                        result.models[#result.models+1] =
                        { id = "empty.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                    end
                    if params.type == 0 then
                        result.models[#result.models+1] =
                        { id = "rheingold_rohrausleger_kurz_v1_18ff5555.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                    elseif params.type == 1 then
                        result.models[#result.models+1] =
                        { id = "rheingold_rohrausleger_kurz_v3_trans.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                    end
                    if params.type == 0 then
                        result.models[#result.models+1] =
                        { id = "rheingold_rohrausleger_kurz_v1_18ff5555_mirror.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance + 5.05, 0.55, 1, } }
                    elseif params.type == 1 then
                        result.models[#result.models+1] =
                        { id = "rheingold_rohrausleger_kurz_v3_trans_mirror.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance + 5.05, 0.55, 1, } }
                    end
            end
            --- 2 oder mehr Gleise ---
            if params.tracks > 0 and params.art ~= 8  and params.poles ~= 4 and params.poles ~= 5 and params.poles ~= 6 then
                if params.tracks > 1 then
                    result.models[#result.models+1] =
                    { id = "rheingold_turmmast_4_5_rohrausleger_3_v1.mdl",
                        transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                else
                    result.models[#result.models+1] =
                    { id = "rheingold_turmmast_3_5_rohrausleger_2_v1.mdl",
                        transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance, 0.55, 1, } }
                end
                if params.holder == 0 then
                    if params.pos == 0 then
                        if params.type == 0 then
                            result.models[#result.models+1] =
                            { id = "rheingold_ausleger_gross_braun_v1.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                            for i = 1,counter,1 do
                                result.models[#result.models+1] =
                                { id = "rheingold_rohrausleger_kurz_v1.mdl",
                                    transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance + (trackDistanceModifier * i), 0.55, 1, } }
                            end
                        elseif params.type == 1 then
                            result.models[#result.models+1] =
                            { id = "rheingold_ausleger_gross_braun_v3.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                            for i = 1,counter,1 do
                                result.models[#result.models+1] =
                                { id = "rheingold_rohrausleger_kurz_v3.mdl",
                                    transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance + (trackDistanceModifier * i), 0.55, 1, } }
                            end
                        end
                    elseif params.pos == 1 then
                        if params.type == 0 then
                            result.models[#result.models+1] =
                            { id = "rheingold_rohrausleger_kurz_v1_18ff5555.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                            for i = 1,counter,1 do
                                result.models[#result.models+1] =
                                { id = "rheingold_rohrausleger_kurz_v1.mdl",
                                    transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance + (trackDistanceModifier * i), 0.55, 1, } }
                            end
                        elseif params.type == 1 then
                            result.models[#result.models+1] =
                            { id = "rheingold_rohrausleger_kurz_v3_trans.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                            for i = 1,counter,1 do
                                result.models[#result.models+1] =
                                { id = "rheingold_rohrausleger_kurz_v3.mdl",
                                    transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance + (trackDistanceModifier * i), 0.55, 1, } }
                            end
                        end
                    end
                elseif params.holder == 1 then
                    if params.type == 0 then
                        result.models[#result.models+1] =
                        { id = "rheingold_rohrausleger_kurz_v1.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                        for i = 1,counter,1 do
                            result.models[#result.models+1] =
                            { id = "rheingold_rohrausleger_kurz_v1.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance + (trackDistanceModifier * i), 0.55, 1, } }
                        end
                    elseif params.type == 1 then
                        result.models[#result.models+1] =
                        { id = "rheingold_rohrausleger_kurz_v3.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
                        for i = 1,counter,1 do
                            result.models[#result.models+1] =
                            { id = "rheingold_rohrausleger_kurz_v3.mdl",
                                transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance + (trackDistanceModifier * i), 0.55, 1, } }
                        end
                    end
                end
                if params.tracks > 2 then
                    if params.pos == 1 then
                        result.models[#result.models+1] =
                        { id = "rheingold_rohrausleger_kurz_v1_18ff5555_mirror.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance + 5.05, 0.55, 1, } }
                    else
                        result.models[#result.models+1] =
                        { id = "rheingold_ausleger_gross_blau_v1_mirror.mdl",
                            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance + 7.55, 0.55, 1, } }
                    end
                end
            end
        end
    elseif params.poles == 1 and params.art ~= 7 then
        result.models[#result.models+1] =
        { id = "rheingold_a_ausrichthilfe.mdl",
            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 7.0, 1, } }
        result.models[#result.models+1] =
        { id = "rheingold_a_ausrichthilfe.mdl",
            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 8.30, 1, } }
    elseif params.art == 7 then
        result.models[#result.models+1] =
        { id = "rheingold_isolator_braun.mdl",
            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset*4, trackDistance+0.05, 7.0, 1, } }
        result.models[#result.models+1] =
        { id = "rheingold_isolator_braun.mdl",
            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset*4, trackDistance+0.05, 8.30, 1, } }
    else
        result.models[#result.models+1] =
        { id = "empty.mdl",
            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 7.0, 1, } }
    end

    if params.poles == 6 then
        result.models[#result.models+1] =
        { id = "rheingold_turmmast_4_5_rohrausleger_3_v1.mdl",
            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance+1.20, 0.55, 1, } }
        result.models[#result.models+1] =
        { id = "rheingold_turmmast_4_5_rohrausleger_3_v1_mirror.mdl",
            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, poleDistance-(trackDistanceModifier*2)-1.20, 0.55, 1, } }
        result.models[#result.models+1] =
        { id = "rheingold_rohrausleger_kurz_v1.mdl",
            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance, 0.55, 1, } }
        result.models[#result.models+1] =
        { id = "rheingold_rohrausleger_kurz_v1.mdl",
            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance+trackDistanceModifier, 0.55, 1, } }
        result.models[#result.models+1] =
        { id = "rheingold_rohrausleger_kurz_v1_mirror.mdl",
            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, -trackDistance-(trackDistanceModifier*3), 0.55, 1, } }
        result.models[#result.models+1] =
        { id = "rheingold_rohrausleger_kurz_v1_mirror.mdl",
            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, -trackDistance-(trackDistanceModifier*2), 0.55, 1, } }
    end


    return result

end

return poles