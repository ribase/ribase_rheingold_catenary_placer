local poles = {}
poles.poles = {}

function poles.createPoles(params)
    local trackDistanceModifier = -5.0
    local trackDistance = -0.05
    local poleDistance = -0.05
    local offset_both
    local offset
    local counter
    local result = { }
    result.models = { }

    if params.ribase_catenary_offset_x == 0 then offset = 1 end;
    if params.ribase_catenary_offset_x == 1 then offset = 0.75 end;
    if params.ribase_catenary_offset_x == 2 then offset = 0.60 end;
    if params.ribase_catenary_offset_x == 3 then offset = 0.45 end;
    if params.ribase_catenary_offset_x == 4 then offset = 0.30 end;
    if params.ribase_catenary_offset_x == 5 then offset = 0.15 end;
    if params.ribase_catenary_offset_x == 6 then offset = 0 end;
    if params.ribase_catenary_offset_x == 7 then offset = -0.15 end;
    if params.ribase_catenary_offset_x == 8 then offset = -0.30 end;
    if params.ribase_catenary_offset_x == 9 then offset = -0.35 end;
    if params.ribase_catenary_offset_x == 10 then offset = -0.60 end;
    if params.ribase_catenary_offset_x == 11 then offset = -0.75 end;
    if params.ribase_catenary_offset_x == 12 then offset = -1 end;

    if params.offset_both == 0 then offset_both = 0.75 end;
    if params.offset_both == 1 then offset_both = 0.60 end;
    if params.offset_both == 2 then offset_both = 0.45 end;
    if params.offset_both == 3 then offset_both = 0.30 end;
    if params.offset_both == 4 then offset_both = 0.15 end;
    if params.offset_both == 5 then offset_both = 0 end;
    if params.offset_both == 6 then offset_both = -0.15 end;
    if params.offset_both == 7 then offset_both = -0.30 end;
    if params.offset_both == 8 then offset_both = -0.35 end;
    if params.offset_both == 9 then offset_both = -0.60 end;
    if params.offset_both == 10 then offset_both = -0.75 end;

    if params.pos == 1 or params.poles == 4 then
        poleDistance = poleDistance + (-1.25)
    end

    trackDistance = trackDistance + offset_both
    poleDistance = poleDistance + offset_both

    offset = offset - 0.15

    counter = params.quertragewerk_gl
    result.models[#result.models+1] =
    { id = "rheingold_bahnhof_quertragewerk_1.mdl",
        transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, 0.0, 0.53, 1, } }
    for i = 1,counter,1
    do
        result.models[#result.models+1] =
        { id = "rheingold_bahnhof_quertragewerk_2.mdl",
            transf = {0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1, 0, offset, trackDistance + (trackDistanceModifier * i), 0.53, 1, } }
    end
    result.models[#result.models+1] =
    { id = "rheingold_bahnhof_quertragewerk_1.mdl",
        transf = {0, -1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, offset,(trackDistanceModifier * (counter + 1)), 0.53, 1, } }

    return result

end

return poles