local params = {}
params.params = {}

function params.createParamsBase(type)

    local paramsArray = {}

    paramsArray[#paramsArray + 1] = {
        key = "ribase_catenary_tension",
        name = _("ribase_catenary_tension"),
        tooltip = _("ribase_catenary_tension_tip"),
        values = {_("strong"), _("normal"), _("weak")},
        defaultIndex = 1
    }

    paramsArray[#paramsArray + 1] = {
        key = "pos",
        name = _("ribase_catenary_position"),
        values = { _("Original"),_("TPF2") },
        defaultIndex = 0
    }

    paramsArray[#paramsArray + 1] = {
        key = "max_length",
        name = _("ribase_catenary_max_length"),
        values = { _("ribase_catenary_max_length_0"),_("ribase_catenary_max_length_1"), _("ribase_catenary_max_length_2") },
        defaultIndex = 0
    }

    paramsArray[#paramsArray + 1] = {
        key = "window",
        name = _("Visibility"),
        values = { "0", "1" },
        uiType = "CHECKBOX",
        defaultIndex = 0
    }

    paramsArray[#paramsArray + 1] = {
        key = "type",
        name = _("ribase_catenary_holder_type"),
        values = {_("Einfach"), _("Doppelt") }
    }

    return paramsArray
end

function params.createParamsSingle()
    local paramsArray = {}
    paramsArray[#paramsArray + 1] = {
        key = "art",
        name = _("ribase_catenary_pole_type"),
        values = { _("ribase_catenary_pole_type_0"),_("ribase_catenary_pole_type_1"),_("ribase_catenary_pole_type_2") ,_("ribase_catenary_pole_type_3") ,_("ribase_catenary_pole_type_4"), _("ribase_catenary_pole_type_5"), _("ribase_catenary_pole_type_6"), _("ribase_catenary_pole_type_7") },
        defaultIndex = 0
    }
    paramsArray[#paramsArray + 1] = {
        key = "poles",
        name = _("ribase_catenary_design"),
        values = { _("ribase_catenary_design_0"),_("ribase_catenary_design_1"), _("ribase_catenary_design_2"), _("ribase_catenary_design_3")},
        defaultIndex = 0
    }

    paramsArray[#paramsArray + 1] = {
        key = "tracks",
        name = _("ribase_catenary_tracks_only_pole"),
        values = {_("1")}
    }
    return paramsArray
end

function params.createParamsOffset()
    local paramsArray = {}

    paramsArray[#paramsArray + 1] = {
        key = "ribase_catenary_offset_x",
        name = _("Offset" ),
        values = { _("6"), _("5"), _("4"),_("3"), _("2"), _("1"), _("0"), _("-1"), _("-2"), _("-3"), _("-4"), _("-5"), _("-6") },
        defaultIndex = 6,
    }

    paramsArray[#paramsArray + 1] = {
        key = "offset_both",
        name = _("ribase_catenary_offset_both"),
        values = { _("5"), _("4"),_("3"), _("2"), _("1"), _("0"), _("-1"), _("-2"), _("-3"), _("-4"), _("-5") },
        defaultIndex = 5
    }

    paramsArray[#paramsArray + 1] = {
        key = "ribase_catenary_offset_y",
        name = _("ribase_catenary_offset_y"),
        tooltip = _("ribase_catenary_offset_y_tip"),
        values = { _("5"), _("4"),_("3"), _("2"), _("1"), _("0"), _("-1"), _("-2"), _("-3"), _("-4"), _("-5") },
        defaultIndex = 5
    }
    paramsArray[#paramsArray + 1] = {
        key = "ribase_catenary_offset_z",
        name = _("ribase_catenary_offset_z"),
        tooltip = _("ribase_catenary_offset_z_tip"),
        values = { _("5"), _("4"),_("3"), _("2"), _("1"), _("0"), _("-1"), _("-2"), _("-3"), _("-4"), _("-5") },
        defaultIndex = 5
    }
    return paramsArray
end

function params.createParamsMulti()
    local paramsArray = {}
    paramsArray[#paramsArray + 1] = {
        key = "art",
        name = _("ribase_catenary_pole_type"),
        values = { _("ribase_catenary_pole_type_0"),_("ribase_catenary_pole_type_1"),_("ribase_catenary_pole_type_2") ,_("ribase_catenary_pole_type_3") ,_("ribase_catenary_pole_type_4"), _("ribase_catenary_pole_type_5"), _("ribase_catenary_pole_type_6"), _("ribase_catenary_pole_type_7") },
        defaultIndex = 0
    }

    paramsArray[#paramsArray + 1] = {
        key = "poles",
        name = _("ribase_catenary_design"),
        values = { _("ribase_catenary_design_0"),_("ribase_catenary_design_1"), _("ribase_catenary_design_2"), _("ribase_catenary_design_3"), _("ribase_catenary_design_4") , _("ribase_catenary_design_5"), _("ribase_catenary_design_6")  },
        defaultIndex = 0
    }

    paramsArray[#paramsArray + 1] = {
        key = "tracks",
        name = _("ribase_catenary_tracks_only_pole"),
        values = {_("X"), _("2"), _("3"), _("4"), _("5"), _("6") },
        defaultIndex = 2
    }
    paramsArray[#paramsArray + 1] = {
        key = "holder",
        name = _("ribase_catenary_holder_type"),
        values = {_("Am Mast"), _("Am Ausleger") },
        defaultIndex = 0
    }
    return paramsArray
end


function params.createParamsBeam()
    local paramsArray = {}
    local trackSize  = {}
    trackSize[#trackSize + 1] = _("x")
    for i = 3, 30 do
        trackSize[#trackSize + 1] = tostring(i)
    end
    paramsArray[#paramsArray + 1] = {
        key = "quertragewerk_gl",
        name = _("ribase_catenary_tracks_only_cross_beam"),
        values = trackSize,
        defaultIndex = 1
    }
    return paramsArray
end

return params