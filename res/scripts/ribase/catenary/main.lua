local transf = require "transf"

local vec2 = require "snowball/common/vec2_1"
local vec3 = require "snowball/common/vec3_1"
local vec4 = require "snowball/common/vec4_1"
local mat3 = require "snowball/common/mat3_1"
local polygon = require "snowball/powerline/polygon"

local powerlines = {}

powerlines.modePlanning = 0
powerlines.modeBuild = 1
powerlines.modeReset = 2

local tensions = { 0.000, 0.0010, 0.0025 }

powerlines.powerlines = {}


function powerlines.buildGround(point, result)

	local p1 = vec3.add(point, {0.2, 0, 0})
	local p2 = vec3.add(point, {0, 0.2, 0})
	local p3 = vec3.add(point, {-0.2, 0, 0})
	local p4 = vec3.add(point, {0, -0.2, 0})

	result.groundFaces[#result.groundFaces + 1] = {
		face = {p1, p2, p3, p4},
		modes = {
			{
				type = "FILL",
				key = "tree_ground.lua"
			},
		}
	}
end

function powerlines.buildSegment(point1, point2, model, modelTransform, result)
	local b2 = vec3.sub(point2, point1)
	local b3 = vec2.mul(2, vec2.normalize({b2[2], -b2[1]}))
	b3[3] = 0

	local affine = mat3.affine(b2, b3)

	local transform =
	transf.mul(
		transf.new(
			vec4.new(affine[1][1], affine[2][1], affine[3][1], .0),
			vec4.new(affine[1][2], affine[2][2], affine[3][2], .0),
			vec4.new(affine[1][3], affine[2][3], affine[3][3], .0),
			vec4.new(point1[1], point1[2], point1[3], 1.0)
		),
		modelTransform
	)

	result.models[#result.models + 1] = {
		id = model,
		transf = transform
	}
end

function powerlines.buildCable(transf1, transf2, offset1, offset2, configuration1, configuration2, model, tension, result, maxlength)
	local thickness = configuration2[4]

	local a = vec3.add(transf1[4],mat3.transform(transf1,{configuration1[2] + offset1[2],configuration1[3] + offset1[3],configuration1[1] - offset1[1]}))
	local b = vec3.add(transf2[4],mat3.transform(transf2,{configuration2[2] + offset2[2],configuration2[3] + offset2[3],configuration2[1] - offset2[1]}))

	local d = vec2.length(vec2.sub(b, a))

	local length = 50

	if maxlength == 0 then
		length = 50 + 0.1
	elseif maxlength == 1 then
		length = 60 + 0.1
	elseif maxlength == 2 then
		length = 80 + 0.1
	end

	if d > 0.01 and d < length then
		local parabel = {tensions[tension], 0, -tensions[tension] * 0.25 * d * d}

		local segment_count = math.min(20, math.max(1, math.round(d / 8)))
		local segment_length = d / segment_count
		local cable = {}
		local holder = {}
		local holderb = {}

		for j = 0, segment_count do
			local dx = j * segment_length
			local p = vec3.add(a, vec3.mul(dx / d, vec3.sub(b, a)))
			local x = vec3.add(a, vec3.mul(dx / d, vec3.sub(b, a)))
			local z = {0.01, 0.01, 1.30}

			p[3] = p[3] + polygon.parabelPoint(parabel, dx - 0.5 * d)
			cable[#cable + 1] = p

			x[3] = x[3] + polygon.parabelPoint(parabel, dx - 0.5 * d)
			z[3] = z[3] + polygon.parabelPoint(parabel, dx - 0.5 * d)

			holder[#holder + 1] = x
			holderb[#holderb + 1] = vec3.sub(x,z)
		end

		local transf = {1, 0, 0, 0, 0, thickness, 0, 0, 0, 0, thickness, 0, 0, 0, 0, 1}
		local transf2 = {1, 0, 0, 0, 0, thickness, 0, 0, 0, 0, thickness, 0, 0, 0, 0, 1}

		for j = 1, #cable - 1 do
			local a = cable[j]
			local b = cable[j + 1]

			powerlines.buildSegment(a, b, model,transf, result)
		end

		if tension > 1 then
			for j = 1, #holder - 1 do
				if j ~= 1 then
					local a = holder[j]
					local b = holderb[j]
					powerlines.buildSegment(a, b, model,transf2, result)
				end
			end
		end
	end
end


function powerlines.build(new, previous)
	local counter
	local trackDistanceModifier = -5.0
	local trackDistance = 5
	local skipMid

	if not new or not previous then
		return
	end

	if new.poles == 2 or previous.poles == 2 then
		return
	end

	if new.art == 3 then
		counter = new.tracks+1
	elseif new.poles == 6 then
		skipMid = true
		trackDistance = trackDistance + (trackDistance*2)
		counter = new.tracks
	else
		counter = new.tracks
	end


	local result = {
		models = {},
		terrainAlignmentLists = {
			{
				type = "EQUAL",
				faces = {}
			}
		},
		groundFaces = {},
	}

	for i = 1,counter,1 do
		local trackDistanceModifierStation = 0
		if skipMid then
			if i == 1 or i == 2 then
				trackDistanceModifierStation = -trackDistanceModifier
			end
		end



		local configurationA = {{7, 0, trackDistance + trackDistanceModifierStation + (trackDistanceModifier * i) + new.offset_both, 0.3 },{8.30, 0, trackDistance + trackDistanceModifierStation + (trackDistanceModifier * i) + new.offset_both, 0.3 } }
		local configurationB = {{7, 0, trackDistance + trackDistanceModifierStation + (trackDistanceModifier * i) + previous.offset_both , 0.3 },{8.30, 0, trackDistance + trackDistanceModifierStation + (trackDistanceModifier * i) + previous.offset_both, 0.3 } }

		local shorterResult = #configurationA
		if(shorterResult > #configurationB) then
			shorterResult = #configurationB
		end

		for k = 1, shorterResult do
			local tension = new.tension
			if k == 1 then
				tension = 1
			end
			powerlines.buildCable(
				previous.transf,
				new.transf,
				previous.offset,
				new.offset,
				configurationB[k],
				configurationA[k],
				"asset/snowball/powerline/snowball_powerline_cable.mdl",
				tension,
				result,
				new.max_length
			)
		end
	end



	powerlines.buildGround(new.position, result)

	local id = nil

	if #result.models > 0 then


		-- local proposal = api.type.SimpleProposal.new()

		-- local construction = api.type.SimpleProposal.ConstructionEntity.new()
		-- construction.fileName = "asset/snowball_powerline_off.con"
		-- construction.params.result = result

		-- proposal.constructionsToAdd[1] = construction

		-- local context = api.type.Context.new()
		-- context.player = api.engine.util.getPlayer()

		-- local callback = function(res, success)
		-- print(res)
		-- print(success)
		-- id = res.entities[1]
		-- end

		-- local cmd = api.cmd.make.buildProposal(proposal, context, false)

		-- api.cmd.sendCommand(cmd, callback)

		-- while id == nil do
		-- end

		id = game.interface.buildConstruction(
			"asset/snowball_powerline_off.con",
			{result = result},
			{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1})

		game.interface.setPlayer(id, api.engine.util.getPlayer())

	end

	return id
end

return powerlines
