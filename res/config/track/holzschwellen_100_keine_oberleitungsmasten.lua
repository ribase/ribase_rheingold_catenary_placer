function data()
	local t = { }

	t.name = _("Test Wood")
	t.desc = _("Test Old")
	t.categories = {"03_keine_masten_holz"}

	t.yearFrom = 1850
	t.yearTo = 0

	t.shapeWidth = 4.0
	t.shapeStep = 4.0
	t.shapeSleeperStep = 8.0 / 13.0

	t.ballastHeight = .3
	t.ballastCutOff = .1

	t.sleeperBase = t.ballastHeight
	t.sleeperLength = .24
	t.sleeperWidth = 2.6
	t.sleeperHeight = .08
	t.sleeperCutOff = .02

	t.railTrackWidth = 1.435
	t.railBase = t.sleeperBase + t.sleeperHeight
	t.railHeight = .15
	t.railWidth = .07
	t.railCutOff = .02
    
    t.embankmentSlopeLow = 0.75
    t.embankmentSlopeHigh = 2.5

	t.catenaryBase = 5.917 + t.railBase + t.railHeight
	t.catenaryHeight = 1.35
	t.catenaryPoleDistance = 50.0 	t.catenaryMaxPoleDistanceFactor = 1.0 	t.catenaryMinPoleDistanceFactor = 0.35

	t.trackDistance = 5.0

	t.speedLimit = 100.0 / 3.6
	t.speedCoeffs = { .9, 15.0, .63 }

	t.ballastMaterial = "track/rheingold_ballast_old.mtl"
	t.sleeperMaterial = "track/rheingold_sleeper_wood.mtl"
	t.railMaterial = "track/rail.mtl"
	t.catenaryMaterial = "empty.mtl"
	t.tunnelWallMaterial = "track/tunnel_rail_ug.mtl"
	t.tunnelHullMaterial = "track/tunnel_hull.mtl"

	t.catenaryPoleModel = "empty.mdl"
	t.catenaryMultiPoleModel = "empty.mdl"
	t.catenaryMultiGirderModel = "empty.mdl"
	t.catenaryMultiInnerPoleModel = "empty.mdl"

	t.bumperModel = "railroad/bumper.mdl"
	t.switchSignalModel = "railroad/switch_box.mdl"

	t.fillGroundTex = "ballast_fill_stone.lua"
	t.borderGroundTex = "ballast.lua"

	t.railModel ="railroad/tracks/single_rail.mdl"
	t.sleeperModel = "railroad/rheingold_tracks_wood/rheingold_sleeper_wood.mdl"
	t.trackStraightModel = {
		"railroad/rheingold_tracks_wood/rheingold_track_wood_2m_base.mdl",
		"railroad/rheingold_tracks_wood/rheingold_track_wood_4m_base.mdl",
		"railroad/rheingold_tracks_wood/rheingold_track_wood_8m_base.mdl",
		"railroad/rheingold_tracks_wood/rheingold_track_wood_16m_base.mdl",
	}
	
	t.cost = 150.0

	return t
end