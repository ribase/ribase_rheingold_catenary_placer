local powerlines = require "ribase/catenary/main"
local mat3 = require "snowball/common/mat3_1"
local vec3 = require "snowball/common/vec3_1"
local dump = require "snowball/common/dump_1"

local state = {
    previous = {},
    preview = nil
}
local function getPowerline(params)

	-- fetch construction input
	-- fetch construction input
    local added = params.proposal.toAdd[1]
    local tracks
    local art = added.params.art
    local poles = added.params.poles
	if not added then
		return nil
	end
	
    local powerline = string.match(added.fileName, "^.+/(.+)%.con$")

    debugPrint(powerlines.powerlines)
    if not powerlines.powerlines[powerline] then
        return
    end

    local position = {added.transf[13], added.transf[14], added.transf[15]}
    	
	local transf = {
		{added.transf[1], added.transf[5], added.transf[9], added.transf[4]},
		{added.transf[2], added.transf[6], added.transf[10], added.transf[8]},
		{added.transf[3], added.transf[7], added.transf[11], added.transf[12]},
		{added.transf[13], added.transf[14], added.transf[15], added.transf[16]},
	}

	local tension = powerlines.powerlines[powerline][1].tension

	if tension == nil then
		tension = added.params.ribase_catenary_tension + 1
	end

	local offset = {0,0,0 }

	if added.params.ribase_catenary_offset_z then
		offset[1] = added.params.ribase_catenary_offset_z
    end

	if added.params.ribase_catenary_offset_y then
		offset[3] = added.params.ribase_catenary_offset_y
    end

	if added.params.ribase_catenary_offset_x then
		offset[2] = added.params.ribase_catenary_offset_x
    end

    local offset_both = added.params.offset_both
    local max_length = added.params.max_length

	local offsetCoeffs = powerlines.powerlines[powerline][1].offset
    local window = added.params.window

    tracks = added.params.tracks + 1
    if powerline == "ribase_catenary_rheingold_ol_beam" then
        tracks = added.params.quertragewerk_gl + 2
    end

	offset[1] = 5 * offsetCoeffs[3] - offset[1] * offsetCoeffs[3]
	offset[2] = 5 * offsetCoeffs[1] - offset[2] * offsetCoeffs[1]
	offset[3] = 5 * offsetCoeffs[2] - offset[3] * offsetCoeffs[2]
    offset_both = 5 * offsetCoeffs[2] - offset_both * offsetCoeffs[2]

    local result = {
        powerline = powerlines.powerlines[powerline][1],
		tension = tension,
        position = position,
		transf = transf,
		offset = offset,
        tracks = tracks,
        art = art,
        offset_both = offset_both,
        poles = poles,
        window = window,
        max_length = max_length
    }
    if params.result and #params.result > 0 then
        result.id = params.result[1]
    end

    return result
end

function data()
    return {      
		handleEvent = function(src, id, name, param)
            if id ~= "__powerlinesEvent__" or src ~= "ribase_rheingold_catenary_placer_callback.lua" then
                return
            end
            
            if state.preview and game.interface.getEntity(state.preview) then
                game.interface.bulldoze(state.preview)
                state.preview = nil
            end
           
            if name == "reset" then
                state.previous = nil                
            end
			
            if name == "build" then
				local cables = powerlines.build(param, state.previous)
				local player = game.interface.getPlayer()
				if cables ~= nil then
					game.interface.setPlayer(cables, player)
                end
				state.previous = param                
            end

            if name == "preview" and state.previous then
                state.preview = powerlines.build(param, state.previous)
            end
           
        end,        
        guiHandleEvent = function(id, name, param)
			--The user navigated away from the powerline-tab.
            if name == "visibilityChange" and param == false then

                local powerline = string.match(id, "^.+/(.+)%.con$")
                if not powerline or not powerlines.powerlines[powerline] then
                    return
                end
                
				game.interface.sendScriptEvent("__powerlinesEvent__", "reset", {})
            --The user either built a fence segment or is previewing it
			elseif name == "builder.apply" or name == "builder.proposalCreate" and id == "constructionBuilder" then
				local params = getPowerline(param)
				if not params then
					return
				end
                
                if name == "builder.apply" then
                    game.interface.sendScriptEvent("__powerlinesEvent__", "build", params)
                else
                    game.interface.sendScriptEvent("__powerlinesEvent__", "preview", params)
                end
			end
        end
    }
end
