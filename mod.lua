local function pRequire(modulName)
	 local state, result_or_error = pcall(require, modulName)
	 if state then
	 return result_or_error
	 else
	 return nil, result_or_error
	 end
end

function data()
  return {
	info = {
		minorVersion = 0,
		severityAdd = "NONE",
		severityRemove = "CRITICAL", 
		name = _("title"),
		description = _("description"),
		tags = { "Brush Asset" },
		requiredMods = {
			{
				modId= "snowball_powerline_1", -- optional, modid
				minMinorVersion = 0 -- optional, minimale minorVersion
			},
			{
				modId= "rheingold_db_gleisset_mod_1", -- optional, modid
				minMinorVersion = 0 -- optional, minimale minorVersion
			},
		},
		visible = true,		
			
			authors = {	
				{
					name = 'Ribase',
					role = 'CREATOR',
				},
				{
					name = 'Yoshi',
					role = 'SCRIPT',
				},
				{
					name = 'Snowball',
					role = 'SCRIPT',
				},
				{
					name = 'Rheingold',
					role = '3D',
				},
			},
	},
	runFn = function(settings)
		local powerlines = pRequire("ribase/catenary/main")

		local configurationA = {}
		local configurationB = {}
		local random
		if ((type(powerlines) == "table") and (type(powerlines.powerlines) == "table")) then
			powerlines.powerlines["ribase_catenary_rheingold_ol"] = {
				{
					model = "empty.mdl",
					width = 3,
					offset = {0.15,0.15,0.15}, -- Offsetintervalls for parameters X,Y,Z(only in first variant per group needed)
					configurationA,
					configurationB,
				},
			}
			powerlines.powerlines["ribase_catenary_rheingold_ol_beam"] = {
				{
					model = "empty.mdl",
					width = 3,
					offset = {0.15,0.15,0.15}, -- Offsetintervalls for parameters X,Y,Z(only in first variant per group needed)
					configurationA,
					configurationB,
				},
			}
			powerlines.powerlines["ribase_catenary_rheingold_ol_multi"] = {
				{
					model = "empty.mdl",
					width = 3,
					offset = {0.15,0.15,0.15}, -- Offsetintervalls for parameters X,Y,Z(only in first variant per group needed)
					configurationA,
					configurationB,
				},
			}
		end	
	end
	}
	
end
